#include <Arduino.h>

#define sensor2 A1
#define sensor3 A2 // ở giữa
#define sensor4 A3
#define sensor5 A4      // ngoài cùng bên phải
const int echoPin = 12; // cảm biến siêu âm
const int trigPin = 11;
#define in1 3 // bánh phải
#define in2 4
#define in3 7 // bánh trái
#define in4 8
#define enA 5 // pwm bánh phải
#define enB 6 // pwm bánh trái
#define speed 130
#define room1 A5
#define room2 A0
#define readRoom1 9
#define readRoom2 10

byte s1Val;
byte s2Val;
byte s3Val;
int distance;
char command;

boolean status = false;
int intersectionCount = 0;

void forward(void);
void backward(void);
void rightTurn(void);
void leftTurn(void);
void stopMotors(void);
void testSR04(void);
void adjustSpeed(void);
void turnLeft(void);
void turnRight(void);
int getDistance();

void setup()
{
  Serial.begin(9600);
  pinMode(LED_BUILTIN,OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  pinMode(sensor2, INPUT);
  pinMode(sensor3, INPUT);
  pinMode(sensor4, INPUT);
  pinMode(readRoom1,INPUT_PULLUP);
  pinMode(readRoom2,INPUT_PULLUP);

  pinMode(room1, INPUT);
  pinMode(room2, INPUT);

  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  analogWrite(enA, 0); // Tốc độ ban đầu là 0
  analogWrite(enB, 0); // Tốc độ ban đầu là 0
  digitalWrite(LED_BUILTIN,HIGH);
  digitalWrite(trigPin, LOW);
  delay(500);
}

void loop()
{
  digitalWrite(LED_BUILTIN,HIGH);
  if (digitalRead(readRoom1)==HIGH)
  {
    command = 'A';
    Serial.print("mode A");
  }
  else if (digitalRead(readRoom2) == HIGH) {
    command = 'B';
    Serial.print("mode B");
  }
  else {
    command = 'Z';
  }
  switch (command)
  {
  case 'A':
    status = true;
    while (status)
    {
      digitalWrite(LED_BUILTIN,LOW);
      s1Val = digitalRead(sensor2);
      s2Val = digitalRead(sensor3);
      s3Val = digitalRead(sensor4);
      distance = getDistance();
      forward();
      if (s1Val == HIGH && s3Val == HIGH)
      {
        intersectionCount++;
        if (intersectionCount == 1)
        {
          stopMotors();
          delay(500);
          forward();
          delay(300);
          stopMotors();
          delay(500);
          while (digitalRead(sensor3) == LOW)
          {
            turnLeft();
          }
          stopMotors();
          delay(300);
        }
      }
      else if (s1Val == LOW && s3Val == LOW)
      {
        forward();
      }
      else if (s1Val == LOW && s3Val == HIGH)
      {
        adjustSpeed();
      }
      else if (s1Val == HIGH && s3Val == LOW)
      {
        adjustSpeed();
      }
      if (distance < 5)
      {
        stopMotors();
        delay(500);
      }
      if (intersectionCount == 2)
      {
        stopMotors();
        while (digitalRead(room1)==LOW);
        delay(500);
        Serial.print("\"Room1\":\"Received\"");
        command = 'Z';
        intersectionCount = 0;
        status = false;
      }
    }
    break;
  case 'B':
    status = true;
    while (status)
    {
      digitalWrite(LED_BUILTIN,LOW);
      s1Val = digitalRead(sensor2);
      s2Val = digitalRead(sensor3);
      s3Val = digitalRead(sensor4);
      distance = getDistance();
      forward();
      if (s1Val == HIGH && s3Val == HIGH)
      {
        intersectionCount++;
        if (intersectionCount == 1)
        {
          stopMotors();
          delay(500);
          forward();
          delay(300);
          stopMotors();
          delay(500);
          while (digitalRead(sensor3) == LOW)
          {
            turnRight();
          }
          stopMotors();
          delay(300);
        }
      }
      else if (s1Val == LOW && s3Val == LOW)
      {
        forward();
      }
      else if (s1Val == LOW && s3Val == HIGH)
      {
        adjustSpeed();
      }
      else if (s1Val == HIGH && s3Val == LOW)
      {
        adjustSpeed();
      }
      if (distance < 5)
      {
        stopMotors();
        delay(500);
      }
      if (intersectionCount == 2)
      {
        stopMotors();
        while (digitalRead(room2)==LOW);
        delay(500);
        Serial.print("\"Room2\":\"Received\"");
        command = 'Z';
        intersectionCount = 0;
        status = false;
      }
    }
    break;
  default:
    stopMotors();
  }
}

void adjustSpeed()
{
  s1Val = digitalRead(sensor2);
  s2Val = digitalRead(sensor3);
  s3Val = digitalRead(sensor4);
  int speedDifference = speed;
  int baseSpeed = speed;
  if (s1Val == LOW && s3Val == HIGH)
  { // lệch trái
    analogWrite(enB, baseSpeed);
    analogWrite(enA, baseSpeed - speedDifference);
  }
  else if (s1Val == HIGH && s3Val == LOW)
  { // lệch phải
    analogWrite(enB, baseSpeed - speedDifference);
    analogWrite(enA, baseSpeed);
  }
  else if (s1Val == LOW && s3Val == LOW)
  { // giữa
    forward();
  }
  else
  {
    stopMotors();
  }
}

void turnLeft()
{
  int turnSpeed = 170;
  analogWrite(enA, turnSpeed);
  analogWrite(enB, turnSpeed);
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  delay(50);
}

void turnRight()
{
  int turnSpeed = 170;
  analogWrite(enA, turnSpeed);
  analogWrite(enB, turnSpeed);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  delay(50);
}

int getDistance()
{
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  long duration = pulseIn(echoPin, HIGH);
  float distance_cm = (duration / 2.0) * 0.0343;
  return distance_cm;
}

void testSR04()
{
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  long duration = pulseIn(echoPin, HIGH);
  float distance_cm = (duration / 2.0) * 0.0343;
  Serial.print("Khoang cach: ");
  Serial.print(distance_cm);
  Serial.println(" cm");
  delay(1000);
}

void forward()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void backward()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void rightTurn()
{
  digitalWrite(in1, HIGH);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, HIGH);
  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void leftTurn()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  digitalWrite(in3, HIGH);
  digitalWrite(in4, LOW);
  analogWrite(enA, speed);
  analogWrite(enB, speed);
}

void stopMotors()
{
  digitalWrite(in1, LOW);
  digitalWrite(in2, LOW);
  digitalWrite(in3, LOW);
  digitalWrite(in4, LOW);
  analogWrite(enA, 0);
  analogWrite(enB, 0);
}